#!/bin/bash

set -eux

SETUP_MIRROR='http://mirrors.kernel.org/archlinux/$repo/os/$arch'

# update system date/time

timedatectl set-ntp true

# proxy?

read -p "proxy? (y/N)?"
if [[ $REPLY == [yY] ]] ; then
  echo "use proxy"
  proxy=true
  read -p "http_proxy:" http_proxy_field
  read -p "https_proxy:" https_proxy_field
else
  echo "no proxy"
  proxy=false
  http_proxy_field=
  https_proxy_field=
fi

export http_proxy=${http_proxy_field}
export https_proxy=${https_proxy_field}

# drive
read -p "disk nv based? (y/N)?"
if [[ $REPLY == [yY] ]] ; then
  echo "use nv"
  nv_use=true
  read -p "nv drive (e.g., nvme0n1):" setup_disk
  boot_part=${setup_disk}p1
  swap_part=${setup_disk}p2
  root_part=${setup_disk}p3
else
  echo "use sd"
  nv_use=false
  read -p "sd drive (e.g., sda):" setup_disk
  boot_part=${setup_disk}1
  swap_part=${setup_disk}2
  root_part=${setup_disk}3
fi

# encrypt?
read -p "encrypt? (y/N)?"
if [[ $REPLY == [yY] ]] ; then
  echo "use encrypt"
  encrypt=true
  swap_label=cryptswap
  root_label=cryptroot
  root_device=/dev/mapper/root
  swap_device=/dev/mapper/swap
else
  echo "no encryption"
  encrypt=false
  swap_label=swap
  root_label=root
  root_device=/dev/${root_part}
  swap_device=/dev/${swap_part}
fi

# partition disk

parted -s /dev/"${setup_disk}" -- \
  mklabel gpt \
  mkpart primary 0% 2GiB \
  mkpart primary 2GiB 4GiB \
  mkpart primary 4GiB 100% \
  set 1 esp on \
  name 1 boot \
  name 2 $swap_label \
  name 3 $root_label

partprobe /dev/"${setup_disk}"

# format EFI partition

mkfs.fat -n ESP -F 32 /dev/"${boot_part}"

# create & format encrypted root partition

if $encrypt; then
  cryptsetup luksFormat /dev/"${root_part}" --type luks --cipher aes-xts-plain64 --key-size 256 --hash sha256 --use-urandom --verify-passphrase --batch-mode
  cryptsetup open /dev/"${root_part}" root --type luks --allow-discards
fi

# create and format xfs partitions

mkfs.xfs -f -L root ${root_device}
mkdir /mnt/xfs-root
mount -o defaults,noatime ${root_device} /mnt/xfs-root

# mount filesystems
mkdir -p /mnt/xfs-root/boot
mount /dev/${boot_part} /mnt/xfs-root/boot

# setup encrypted swap partition

if $encrypt; then
  dd if=/dev/urandom of=/mnt/xfs-root/root/swap.key bs=1024 count=4
  chmod 0400 /mnt/xfs-root/root/swap.key
  cryptsetup open /dev/"${swap_part}" swap --type plain --cipher aes-xts-plain64 --key-size 256 --key-file /mnt/root/swap.key  --batch-mode
fi

mkswap -L swap ${swap_device}

# set up pacman mirrorlist

echo "Server = ${SETUP_MIRROR}" > /etc/pacman.d/mirrorlist

# refresh pacman
echo "refresh pacman"
pacman -Sy
pacman -S --noconfirm rsync reflector
reflector -f 6 -l 6 --save /etc/pacman.d/mirrorlist
pacman -Syy

# install PGP keyring

pacman -S --noconfirm archlinux-keyring

# install basic packages

pacstrap /mnt/xfs-root base base-devel linux linux-firmware grub efibootmgr os-prober dosfstools mtools gptfdisk

# create /etc/fstab
genfstab -U -p /mnt/xfs-root >> /mnt/xfs-root/etc/fstab

# continue in chroot

cp install-arch-chroot-xfs-sb.sh /mnt/xfs-root/root/install-arch-chroot-xfs-sb.sh
arch-chroot /mnt/xfs-root /root/install-arch-chroot-xfs-sb.sh ${encrypt}
rm /mnt/xfs-root/root/install-arch-chroot-xfs-sb.sh

# setup systemd-resolved DNS resolver

ln -sf /run/systemd/resolve/stub-resolv.conf /mnt/xfs-root/etc/resolv.conf

# unmount chroot

umount -R {/mnt/xfs-root}
if $encrypt; then
  cryptsetup close swap
  cryptsetup close root
fi
