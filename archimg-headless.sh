#!/bin/bash
# truncate -s 8GiB arch_disk.img
# sudo pacman -S qemu virt-manager virt-viewer dnsmasq vde2 bridge-utils openbsd-netcat
# sudo pacman -S ebtables iptables
# trizen -S --noconfirm --needed libguestfs
# sudo systemctl enable libvirtd.service
# sudo systemctl start libvirtd.service
# sudo vim /etc/libvirt/libvirtd.conf
#   unix_sock_group = "libvirt"
#   unix_sock_rw_perms = "0770"
# sudo usermod -a -G libvirt $(whoami)
# newgrp libvirt
# sudo vim /etc/libvirt/qemu.conf
#   nvram = [
#   "/usr/share/ovmf/OVMF_CODE.fd:/usr/share/ovmf/OVMF_VARS.fd"
#   ]
# Above is deprecated
# sudo mkdir -p /etc/qemu
# sudo cp /usr/share/qemu/firmware/60-edk2-ovmf-x86_64.json /etc/qemu
# sudo systemctl restart libvirtd.service
# sudo modprobe -r kvm_intel
# sudo modprobe kvm_intel nested=1
# echo "options kvm-intel nested=1" | sudo tee /etc/modprobe.d/kvm-intel.conf

tdir=$(mktemp -d)
iso=~/archlinux-2020.04.01-x86_64.iso

isoinfo -i$iso -x'/ARCH/BOOT/X86_64/VMLINUZ.;1' >${tdir}/vmlinuz
isoinfo -i$iso -x'/ARCH/BOOT/X86_64/ARCHISO.IMG;1' >${tdir}/archiso.img

virt-install -n arch --memory=2048 \
  --boot uefi \
  --location ${tdir},kernel=${tdir}/vmlinuz,initrd=${tdir}/archiso.img \
  --disk $iso,device=cdrom --disk ~/archinstall/arch_disk.img,size=8 --nographics \
  --install kernel_args='console=tty0 console=ttyS0,115200n8 serial archisodevice=/dev/sr0'
