#!/bin/bash

set -eux

SETUP_TIMEZONE=America/Los_Angeles
SETUP_LOCALE=en_US
SETUP_HOSTNAME=dev
SETUP_USER=seanvk
SETUP_GATEWAY=192.168.1.1
SETUP_WIFI=wl*
SETUP_WIRED=en*
USE_ENCRYPT=$1
KEYMAP=us

# date/time

ln -sf /usr/share/zoneinfo/"${SETUP_TIMEZONE}" /etc/localtime
hwclock --systohc

# Not supported in chroot (good post install)
#timedatectl set-timezone "${SETUP_TIMEZONE}"
#timedatectl set-local-rtc false
#timedatectl set-ntp true

# locale

echo "${SETUP_LOCALE}.UTF-8 UTF-8" > /etc/locale.gen
echo "LC_ALL=${SETUP_LOCALE}.UTF-8" > /etc/locale.conf
echo "LC_CTYPE=${SETUP_LOCALE}.UTF-8" > /etc/locale.conf
echo "LANG=${SETUP_LOCALE}.UTF-8" > /etc/locale.conf
locale-gen

# keyboard

echo "KEYMAP=$KEYMAP" > /etc/vconsole.conf

# Not supported in chroot (good post install)
#localectl set-locale ${SETUP_LOCALE}.UTF-8
#localectl set-keymap ${KEYMAP}

# hostname
echo "${SETUP_HOSTNAME}" > /etc/hostname

cat > /etc/hosts << EOF
127.0.0.1 localhost
::1       localhost
127.0.1.1 ${SETUP_HOSTNAME}.localdomain ${SETUP_HOSTNAME}
EOF

# Not supported in chroot (good post install)
#hostnamectl set-hostname "${SETUP_HOSTNAME}"

# pacman set up

pacman -S --noconfirm rsync reflector
reflector -f 6 -l 6 --save /etc/pacman.d/mirrorlist
pacman -Syy

# install extra packages & remove unneeded ones

pacman -S --noconfirm man vim openssh git colordiff efitools sbsigntools \
  intel-ucode xfsprogs xfsdump btrfs-progs bash-completion pacman-contrib powertop

pacman -S --noconfirm tmux stow zsh grml-zsh-config zsh-doc zsh-autosuggestions \
  zsh-completions zsh-history-substring-search zsh-lovers zsh-syntax-highlighting

pacman -S --noconfirm tmux htop wget wireless_tools iwd wpa_supplicant

pacman -S --noconfirm xdg-user-dirs

pacman -S --noconfirm cpupower nss elinks weechat dhclient

pacman -S --noconfirm alsa-utils dnsutils rfkill

pacman -S --noconfirm nss-mdns \
  fuse exfat-utils ntp acpid acpica cracklib keychain

pacman -S --noconfirm xmlto kmod inetutils bc libelf

pacman -S --noconfirm openbsd-netcat tsocks linux-headers \
  dkms gnupg

# make console font larger

cat > /etc/vconsole.conf << EOF
FONT=latarcyrheb-sun32
EOF


# initramfs hook for opening encrypted swap

if $USE_ENCRYPT ; then

cat > /etc/initcpio/install/openswap << EOF
build ()
{
   add_runscript
}
help ()
{
  echo "Hook for opening encrypted swap partition"
}
EOF

cat > /etc/initcpio/hooks/openswap << EOF
run_hook ()
{
  mkdir openswap_keymount
  mount -o ro,subvol=/ /dev/mapper/root openswap_keymount
  cryptsetup open /dev/disk/by-partlabel/cryptswap swap --type plain --cipher aes-xts-plain64 --key-size 256 --key-file openswap_keymount/swap.key --allow-discards
  umount openswap_keymount
}
EOF

fi

# microcode

echo 'microcode' > /etc/modules-load.d/intel-ucode.conf

# initramfs configuration

sed -i 's/^\(MODULES=\).*$/\1(i915)/' /etc/mkinitcpio.conf
sed -i 's/^\(BINARIES=\).*$/\1("\/usr\/bin\/btrfs")/' /etc/mkinitcpio.conf
if $USE_ENCRYPT ; then
  sed -i 's/^\(HOOKS=\).*$/\1(base udev autodetect keyboard consolefont modconf block encrypt openswap resume filesystems fsck)/' /etc/mkinitcpio.conf
else
  sed -i 's/^\(HOOKS=\).*$/\1(base udev autodetect keyboard consolefont modconf block resume filesystems fsck)/' /etc/mkinitcpio.conf
fi
sed -i 's/^#\(COMPRESSION="xz"\)$/\1/' /etc/mkinitcpio.conf

echo "options i915 fastboot=1 enable_fbc=1 enable_guc=3 enable_psr=0" > /etc/modprobe.d/i915.conf

# rebuild initramfs & remove fallback

sed -i 's/\(PRESETS=\).*$/\1("default")/' /etc/mkinitcpio.d/linux.preset
mkinitcpio -p linux
rm /boot/initramfs-linux-fallback.img

# install bootloader

bootctl --path=/boot install

cat > /boot/loader/loader.conf << EOF
default ArchLinux
timeout 5
EOF

if $USE_ENCRYPT ; then

cat > /boot/loader/entries/ArchLinux.conf << EOF
title Arch Linux
linux   /vmlinuz-linux
initrd  /intel-ucode.img
initrd  /initramfs-linux.img
options rw quiet loglevel=3 udev.log_priority=3 nowatchdog \
cryptdevice=PARTLABEL=cryptroot:root:allow-discards \
resume=LABEL=swap \
root=LABEL=root
EOF

else

cat > /boot/loader/entries/ArchLinux.conf << EOF
title Arch Linux
linux   /vmlinuz-linux
initrd  /intel-ucode.img
initrd  /initramfs-linux.img
options rw quiet loglevel=3 udev.log_priority=3 nowatchdog \
resume=LABEL=swap \
root=LABEL=root
EOF

fi

# keep only latest 3 versions of packages
mkdir -p /etc/pacman.d/hooks/

cat > /etc/pacman.d/hooks/pacman-cleanup.hook << EOF
[Trigger]
Type = Package
Operation = Remove
Operation = Install
Operation = Upgrade
Target = *

[Action]
Description = Keep only latest 3 versions of packages
When = PostTransaction
Exec = /usr/bin/paccache -rk3
EOF

# create powertop service

cat > /etc/systemd/system/powertop.service << EOF
[Unit]
Description=Powertop tunings

[Service]
ExecStart=/usr/bin/powertop --auto-tune
RemainAfterExit=true

[Install]
WantedBy=multi-user.target
EOF

# enable system services

systemctl enable systemd-timesyncd.service
systemctl enable systemd-networkd.service
systemctl enable systemd-resolved.service
systemctl enable powertop.service
systemctl enable iwd.service
systemctl enable fstrim.timer

# Whenused with systemd-networkd, set order
mkdir -p /etc/systemd/system/iwd.service.d
cat > /etc/systemd/system/iwd.service.d/override.conf << EOF
[Unit]
After=systemd-udevd.service systemd-networkd.service
EOF

# iwd config

mkdir -p /etc/iwd
cat > /etc/iwd/main.conf << EOF
[General]
UseDefaultInterface=false

[Network]
NameResolvingService=systemd
EnableNetworkConfiguration=false
EOF

# iwd setup
# man iwctl
# man iwd.network
# sudo iwctl
#[iwd]# iwctl device list
#[iwd]# station DEVICE scan
#[iwd]# station DEVICE get-networks
#[iwd]# station DEVICE connect SSID
# or
# sudo iwctl --passphrase=PASSPHRASE station DEVICE connect SSID

# PSK file example
# wpa_passphrase SSID PASSPHRASE
# /var/lib/iwd/station.psk
# [Security]
# PreSharedKey=KEY
# note: station name non-alphanumeric must be =hexa
# e.g., Brad=c3=a1nFeasa.psk
# network resolved config

mkdir -p /etc/resolved.conf.d
cat > /etc/resolved.conf.d/dns_servers.conf << EOF
[Resolve]
DNS="${SETUP_GATEWAY}"
Domains=-.
EOF

# netwrok timeout

mkdir -p /etc/systemd/networking.service.d
cat > /etc/systemd/networking.service.d/network-pre.conf << EOF

[Service]
TimeoutStartSec=15
EOF

# systemd-networkd config

cat > /etc/systemd/network/25-wifi.network << EOF
[Match]
Name=${SETUP_WIFI}

[Network]
DHCP=yes

[DHCP]
RouteMetric=20
EOF
cat > /etc/systemd/network/20-wired.network << EOF

[Match]
Name=${SETUP_WIRED}

[Network]
DHCP=ipv4
EOF

# enable systemd stuff
systemctl enable cpupower.service
systemctl enable sshd.service
systemctl enable acpid.service
systemctl enable ntpd.service

# sysctl tweaks

cat > /etc/sysctl.d/tweaks.conf << EOF
# less swapping
vm.swappiness = 0
vm.dirty_ratio = 3
vm.dirty_background_ratio = 2

# no magic-sysrq key
kernel.sysrq = 0

# max connections
net.core.somaxconn = 1024

# network memory limits
net.core.rmem_default = 1048576
net.core.rmem_max = 16777216
net.core.wmem_default = 1048576
net.core.wmem_max = 16777216
net.core.optmem_max = 65536
net.ipv4.tcp_rmem = 4096 1048576 2097152
net.ipv4.tcp_wmem = 4096 65536 16777216
net.ipv4.udp_rmem_min = 8192
net.ipv4.udp_wmem_min = 8192

# set tcp keepalive to 120 sec
net.ipv4.tcp_keepalive_time = 60
net.ipv4.tcp_keepalive_intvl = 10
net.ipv4.tcp_keepalive_probes = 6
EOF

# pacman & makepkg config

sed -i 's/^#\(Color\).*$/\1/'           /etc/pacman.conf
sed -i 's/^#\(TotalDownload\).*$/\1/'   /etc/pacman.conf
sed -i 's/^#\(VerbosePkgLists\).*$/\1/' /etc/pacman.conf

sed -i "s/^#\(MAKEFLAGS=\).*$/\1\"-j`nproc`\"/" /etc/makepkg.conf
sed -i 's/^\(PKGEXT=\).*$/\1".pkg.tar"/'        /etc/makepkg.conf

# sudo config

#CONFIGURE SUDOERS {{{
  if [[ ! -f  /etc/sudoers.orig ]]; then
    cp -v /etc/sudoers /etc/sudoers.orig
    ## Uncomment to allow members of group wheel to execute any command
    #sed -i '/%wheel ALL=(ALL) ALL/s/^#//' /etc/sudoers
    ## Same thing without a password (not secure)
    #sed -i '/%wheel ALL=(ALL) NOPASSWD: ALL/s/^#//' /etc/sudoers

    #This config is especially helpful for those using terminal multiplexers like screen, tmux, or ratpoison, and those using sudo from scripts/cronjobs:
    echo "" >> /etc/sudoers
    echo 'Defaults !requiretty, !tty_tickets, !umask' >> /etc/sudoers
    echo 'Defaults visiblepw, path_info, insults, lecture=always' >> /etc/sudoers
    echo 'Defaults loglinelen=0, logfile =/var/log/sudo.log, log_year, log_host, syslog=auth' >> /etc/sudoers
    echo 'Defaults passwd_tries=3, passwd_timeout=1' >> /etc/sudoers
    echo 'Defaults env_reset, always_set_home, set_home, set_logname' >> /etc/sudoers
    echo 'Defaults !env_editor, editor="/usr/bin/vim:/usr/bin/vi:/usr/bin/nano"' >> /etc/sudoers
    echo 'Defaults timestamp_timeout=15' >> /etc/sudoers
    echo 'Defaults passprompt="[sudo] password for %u: "' >> /etc/sudoers
    echo 'Defaults lecture=never' >> /etc/sudoers
  fi
echo "%wheel ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/wheel
#}}}

# set vim as default editor

echo "export EDITOR=vim" > /etc/profile.d/editor.sh

# non-root user

useradd -g users -G wheel -m -s /bin/zsh ${SETUP_USER}
chfn ${SETUP_USER}
passwd ${SETUP_USER}
while [[ $? -ne 0 ]]; do
  passwd ${SETUP_USER}
done

# disable root password

passwd -l root

# autologin user on boot

mkdir -p "/etc/systemd/system/getty@tty1.service.d"
cat > "/etc/systemd/system/getty@tty1.service.d/override.conf" << EOF
[Service]
ExecStart=
ExecStart=-/usr/bin/agetty --autologin ${SETUP_USER} --noclear %I \$TERM
Type=simple
EOF
