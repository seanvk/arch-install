#!/bin/bash
# truncate -s 8GiB arch_disk.img
# sudo pacman -S qemu virt-manager virt-viewer dnsmasq vde2 bridge-utils openbsd-netcat
# sudo pacman -S ebtables iptables
# trizen -S --noconfirm --needed libguestfs
# sudo systemctl enable libvirtd.service
# sudo systemctl start libvirtd.service
# sudo vim /etc/libvirt/libvirtd.conf
#   unix_sock_group = "libvirt"
#   unix_sock_rw_perms = "0770"
# sudo usermod -a -G libvirt $(whoami)
# newgrp libvirt
# sudo vim /etc/libvirt/qemu.conf
#   nvram = [
#   "/usr/share/ovmf/OVMF_CODE.fd:/usr/share/ovmf/OVMF_VARS.fd"
#   ]
# Above is deprecated
# sudo mkdir -p /etc/qemu
# sudo cp /usr/share/qemu/firmware/60-edk2-ovmf-x86_64.json /etc/qemu
# sudo systemctl restart libvirtd.service
# sudo modprobe -r kvm_intel
# sudo modprobe kvm_intel nested=1
# echo "options kvm-intel nested=1" | sudo tee /etc/modprobe.d/kvm-intel.conf

virt-install \
  --name arch-linux_testing \
  --memory 2048             \
  --boot=uefi      \
  --vcpus=2,maxvcpus=4      \
  --cpu host                \
  --cdrom /home/seanvk/archlinux-2020.04.01-x86_64.iso \
  --disk ~/archinstall/arch_disk2.img,size=8
  --network user            \
  --virt-type kvm
