#!/bin/bash

set -eux

# aur helper yay install it

curl -sfL https://aur.archlinux.org/cgit/aur.git/snapshot/yay-bin.tar.gz | tar xzf -
cd yay-bin && makepkg -si && cd .. && rm -rf yay-bin

# kernel modules hook

yay -S kernel-modules-hook
sudo systemctl daemon-reload
sudo systemctl enable linux-modules-cleanup

# systemd-boot hook

yay -S systemd-boot-pacman-hook

# plymouth

yay -S plymouth ttf-dejavu
sudo sed -i 's/ udev / udev plymouth /' /etc/mkinitcpio.conf
sudo sed -i 's/ encrypt / plymouth-encrypt /' /etc/mkinitcpio.conf
sudo sed -i 's/ quiet / quiet splash /' /boot/cmdline.txt
cat << EOF | sudo tee /etc/plymouth/plymouthd.conf
[Daemon]
Theme=spinfinity
ShowDelay=0
EOF
sudo mkinitcpio -p linux

# install sway
yay -S --needed wlroots-git sway-git i3blocks rofi rofi-dmenu j4-dmenu-desktop qt5-wayland

# install termite and mako

yay -S termite mako

# fonts

yay -S --needed ttf-bitstream-vera ttf-dejavu ttf-liberation ttf-inconsolata adobe-source-han-{sans,serif}-otc-fonts ttf-font-icons
sudo ln -s /etc/fonts/conf.avail/10-sub-pixel-rgb.conf /etc/fonts/conf.d/
sudo ln -s /etc/fonts/conf.avail/11-lcdfilter-light.conf /etc/fonts/conf.d/

# vulkan

yay -S vulkan-icd-loader vulkan-intel

# vaapi

yay -S libva-intel-driver libva-utils
# check if it is working
vainfo

# opencl

yay -S ocl-icd intel-opencl-runtime compute-runtime-bin
# check if it is working
yay -S clinfo
clinfo

# avahi for local network

yay -S --needed avahi nss-mdns
sudo systemctl enable --now avahi-daemon
sudo sed -i 's/ resolve / mdns_minimal [NOTFOUND=return] resolve /' /etc/nsswitch.conf


# udiskie for automounting

yay -S udiskie ntfs-3g exfat-utils f2fs-tools
echo 'ENV{ID_FS_USAGE}=="filesystem|other|crypto", ENV{UDISKS_FILESYSTEM_SHARED}="1"' | sudo tee /etc/udev/rules.d/99-udisks2.rules
echo 'D /media 0755 root root 0 -' | sudo tee /etc/tmpfiles.d/media.conf

# sublime text and submlime merge

curl -sfO https://download.sublimetext.com/sublimehq-pub.gpg
sudo pacman-key --add sublimehq-pub.gpg
sudo pacman-key --lsign-key 8A8F901A
rm sublimehq-pub.gpg
echo -e "\n[sublime-text]\nServer = https://download.sublimetext.com/arch/stable/x86_64" | sudo tee -a /etc/pacman.conf
yay -Syu sublime-text sublime-merge

# misc utilities

yay -S --needed tar cpio bzip2 gzip lrzip lz4 zstd lzip lzop xz p7zip unrar zip unzip
yay -S --needed bc acpi sysstat lsof strace jq fzf ripgrep light nvme-cli

# terminal software

yay -S htop ncdu mosh tmux weechat micro-bin

# FAR manager

yay -S far2l-git

# PulseAudio

yay -S pulseaudio pulseaudio-alsa pulseaudio-bluetooth ponymix pavucontrol-qt

# media software

yay -S mpv youtube-dl ffmpeg-libfdk_aac mkvtoolnix-cli mkclean gpac sox

# network software

yay -S --needed rsync rclone tcpdump nmap socat openbsd-netcat

# Wireguard VPN

yay -S wireguard-dkms wireguard-tools

# Wireshark

yay -S wireshark-qt
sudo gpasswd -a ${USER} wireshark

# Docker

yay -S docker docker-compose
sudo gpasswd -a ${USER} docker
sudo systemctl enable --now docker

# Google Chrome

yay -S google-chrome

# Zathura pdf/djvu reader

yay -S zathura zathura-pdf-mupdf zathura-djvu

# VCS

yay -S --needed git git-lfs tig subversion subversion mercurial

# development tools

yay -S --needed cmake ninja meson clang llvm gdb nemiver nasm
yay -S --needed valgrind perf python-pip python-virtualenv
yay -S --needed intel-gpu-tools renderdoc apitrace vulkan-devel opencl-headers

# Unity3D & Visual Studio Code

yay -S unity-editor visual-studio-code-bin dotnet-runtime dotnet-sdk msbuild-stable mono

# Other software

yay -S pinta gimp dia inkscape calibre
yay -S tor-browser
